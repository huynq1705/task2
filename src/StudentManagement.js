import React, { useState, useEffect } from "react";
import axios from "axios";
import { Table, Pagination, Form, Button } from "react-bootstrap";
import "./reset.css";
import "./StudentManagement.css";

const App = () => {
    const [students, setStudents] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [studentsPerPage, setStudentsPerPage] = useState(10);
    const [totalPages, setTotalPages] = useState(0);

    const [name, setName] = useState("");
    const [gender, setGender] = useState("");
    const [birthday, setBirthday] = useState("");
    const [address, setAddress] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [major, setMajor] = useState("");
    const [gpa, setGpa] = useState("");
    const [status, setStatus] = useState("");

    const [updatingRow, setUpdatingRow] = useState("");
    const [updatingRowID, setUpdatingRowID] = useState(null);

    useEffect(() => {
        const fetchStudents = async () => {
            const response = await axios.get(
                "https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist"
            );
            setStudents(response.data);
            setTotalPages(Math.ceil(response.data.length / studentsPerPage));
        };
        fetchStudents();
    }, []);

    // Render student list
    const indexOfLastStudent = currentPage * studentsPerPage;
    const indexOfFirstStudent = indexOfLastStudent - studentsPerPage;
    const currentStudents = students.slice(
        indexOfFirstStudent,
        indexOfLastStudent
    );
    const renderStudents = currentStudents.map((student) => {
        return (
            <tr key={student.id}>
                <td>{student.id}</td>
                <td>{student.name}</td>
                <td>{student.gender}</td>
                <td>{student.birthday}</td>
                <td>{student.address}</td>
                <td>{student.phone}</td>
                <td>{student.email}</td>
                <td>{student.major}</td>
                <td>{student.gpa}</td>
                <td>{student.status}</td>
                <td>
                    <Button
                        className="btn_inForm"
                        style={{ padding: "0" }}
                        onClick={() => onClickUpdateStudent(student.id)}
                    >
                        <span className="btn-text">Update</span>
                    </Button>
                </td>
                <td>
                    <Button
                        className="btn_inForm"
                        style={{ padding: "0" }}
                        onClick={() => onClickDeleteStudent(student.id)}
                    >
                        <span className="btn-text">Delete</span>
                    </Button>
                </td>
            </tr>
        );
    });

    // Handle pagination
    const handlePaginationClick = (event) => {
        setCurrentPage(Number(event.target.id));
    };

    const pageNumbers = [];
    for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map((number) => {
        return (
            <Pagination.Item
                key={number}
                id={number}
                onClick={handlePaginationClick}
                active={number === currentPage}
            >
                {number}
            </Pagination.Item>
        );
    });

    const onClickAddStudent = async (event) => {
        event.preventDefault();
        const newStudent = {
            name,
            gender,
            birthday,
            address,
            phone,
            email,
            major,
            gpa,
            status,
        };

        await axios.post(
            "https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist",
            newStudent
        );

        // Reset form after submit
        setName("");
        setGender("");
        setBirthday("");
        setAddress("");
        setPhone("");
        setEmail("");
        setMajor("");
        setGpa("");
        setStatus("");
    };

    const onClickUpdate = () => {
        axios.put(
            `https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist/${updatingRowID}`,
            {
                name,
                gender,
                birthday,
                address,
                phone,
                email,
                major,
                gpa,
                status,
            }
        );
        setName("");
        setGender("");
        setBirthday("");
        setAddress("");
        setPhone("");
        setEmail("");
        setMajor("");
        setGpa("");
        setStatus("");
    };
    const onClickUpdateStudent = (id) => {
        axios
            .get(
                `https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist/${id}`
            )
            .then((response) => {
                const data = response.data;
                setUpdatingRow(data);
            });
        setName(updatingRow.name);
        setGender(updatingRow.gender);
        setBirthday(updatingRow.birthday);
        setAddress(updatingRow.address);
        setPhone(updatingRow.phone);
        setEmail(updatingRow.email);
        setMajor(updatingRow.major);
        setGpa(updatingRow.gpa);
        setStatus(updatingRow.status);
        setUpdatingRowID(updatingRow.id);
    };
    const getData = async (id) => {
        const response = await axios.get(
            `https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist`
        );
        setStudents(response.data);
    };
    const onClickDeleteStudent = (id) => {
        if (window.confirm("Bạn chắc chắn muốn xoá học sinh này?")) {
            axios
                .delete(
                    `https://642d91b9bf8cbecdb409cc9d.mockapi.io/studentlist/${id}`
                )
                .then(() => {
                    getData();
                });
            console.log(id);
        }
    };

    return (
        <div className="container">
            <div className="form">
                <h2 className="form_heading">Add Student</h2>
                <Form>
                    <Form.Group controlId="name" className="form_group">
                        <Form.Label className="form_label">Name:</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="text"
                            placeholder="Student Name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="gender" className="form_group">
                        <Form.Label className="form_label">Gender:</Form.Label>
                        <Form.Control
                            className="form-input"
                            as="select"
                            value={gender}
                            onChange={(e) => setGender(e.target.value)}
                        >
                            <option value="">-- Select Gender --</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="birthday" className="form_group">
                        <Form.Label className="form_label">
                            Birthday:
                        </Form.Label>
                        <Form.Control
                            className="form-input"
                            type="date"
                            value={birthday}
                            onChange={(e) => setBirthday(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="address" className="form_group">
                        <Form.Label className="form_label">Address:</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="text"
                            placeholder="Student Address"
                            value={address}
                            onChange={(e) => setAddress(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="phone" className="form_group">
                        <Form.Label className="form_label">Phone:</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="tel"
                            placeholder="Student Phone Number"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="email" className="form_group">
                        <Form.Label className="form_label">Email:</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="email"
                            placeholder="Student Email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="major" className="form_group">
                        <Form.Label className="form_label">Major</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="text"
                            placeholder="Student Major"
                            value={major}
                            onChange={(e) => setMajor(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="gpa" className="form_group">
                        <Form.Label className="form_label">GPA:</Form.Label>
                        <Form.Control
                            className="form-input"
                            type="number"
                            placeholder="Student GPA"
                            value={gpa}
                            onChange={(e) => setGpa(e.target.value)}
                            min="0"
                            max="4"
                            step="0.01"
                        />
                    </Form.Group>

                    <Form.Group controlId="status" className="form_group">
                        <Form.Label className="form_label">Status:</Form.Label>
                        <Form.Control
                            className="form-input"
                            as="select"
                            value={status}
                            onChange={(e) => setStatus(e.target.value)}
                        >
                            <option value="">-- Select Status --</option>
                            <option value="Đang học">Đang học</option>
                            <option value="Đã tốt nghiệp">Đã tốt nghiệp</option>
                            <option value="Thôi học">Thôi học</option>
                        </Form.Control>
                    </Form.Group>
                    {updatingRowID ? (
                        <div className="btn">
                            <Button
                                className="btn_update"
                                variant="primary"
                                type="submit"
                                onClick={(e) => onClickUpdate(e)}
                            >
                                <span className="btn-inner">Update</span>
                            </Button>
                        </div>
                    ) : (
                        <div className="btn">
                            <Button
                                className="btn_add"
                                variant="primary"
                                type="submit"
                                onClick={(e) => onClickAddStudent(e)}
                            >
                                <span className="btn-inner">Add Student</span>
                            </Button>
                        </div>
                    )}
                </Form>
            </div>

            <div className="table">
                <h2 className="table_heading">Student List</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Birthday</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Major</th>
                            <th>GPA</th>
                            <th>Status</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>{renderStudents}</tbody>
                </Table>
                <Pagination
                    style={{ display: "flex", justifyContent: "center" }}
                >
                    {renderPageNumbers}
                </Pagination>
            </div>
        </div>
    );
};

export default App;
