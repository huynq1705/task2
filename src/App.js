import "./App.css";
import "./reset.css";

// import AddStudent from "./components/AddStudent/AddStudent";
import StudentManagement from "./StudentManagement";

function App() {
    return (
        <body>
            <header className="header">
                <h1 className="header-title">Student Management</h1>
            </header>
            <div>
                <StudentManagement />
            </div>
        </body>
    );
}

export default App;
